# Site DUT Informatique 1A

Ce site a été réalisé lors de ma première année de DUT Informatique.\
Il s'agit d'un site vitrine réalisé en HTML et CSS.

## Démonstration
Une démo peut être trouvée [ici](https://www.gabrugiere.net/projets-web/site-DUT-1A).

## Screenshots
[<img src="screenshots/home.png" alt="Video media player" width=720>](https://gitlab.com/gabrugiere/site-1a-dut-informatique/-/blob/master/screenshots/home.png)
