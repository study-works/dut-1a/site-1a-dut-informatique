<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<title>Activité longbow | Association Robin des bois</title>
		<link rel="icon" type="image/jpg" href="images/favicon.jpg" />
		<link rel="stylesheet" href="style/general.css" type="text/css" media="screen" />
	</head>
	<body>
		<header>
				<?php include 'include/header.php'; ?>
		</header>

		<main>
			<div class="social">
				<?php include 'include/social.php'; ?>
			</div>
				
			<div class="aside">
				<?php include 'include/aside.php'; ?>
			</div>
				
			<div class="principal">
				<section>
					<h2>Le tir à l'arc au longbow</h2>
					<p><hr /></p>
					<article>
						<p>Enfin, la dernière activité que nous vous présentons est celle du longbow, un arc d'une pièce très puissant utilisé par les Anglais lors de <a href="https://fr.wikipedia.org/wiki/Guerre_de_Cent_Ans" title="Page wikipédia sur la guerre de cent ans">la guerre de cent ans</a> ou encore <a href="https://fr.wikipedia.org/wiki/Guerre_des_Deux-Roses" title="Page wikipédia sur la guerre des deux roses">la guerre des deux roses</a>. Les séances se passent ici plus généralement en extérieur avec des cibles posées en forêt, ici aussi à différentes distances et de différentes tailles. Quelques séances peuvent se passer aussi en intérieur avec les arcs classiques ou les poulies (en fonction de qui aura l'âme la plus charitable) en fonction de la météo.</p>
					</article>
				</section>
			</div>
		</main>
		
		<footer>
				<?php include 'include/footer.php'; ?>
		</footer>
	</body>
</html>
