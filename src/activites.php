<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<title>Les activités | Association Robin des bois</title>
		<link rel="icon" type="image/jpg" href="images/favicon.jpg" />
		<link rel="stylesheet" href="style/general.css" type="text/css" media="screen" />
	</head>
	<body>
		<header>
				<?php include 'include/header.php'; ?>
		</header>

		<main>
			<div class="social">
				<?php include 'include/social.php'; ?>
			</div>
				
			<div class="aside">
				<?php include 'include/aside.php'; ?>
			</div>
				
			<div class="principal">
				<section>
					<h2>Nos activités</h2>
					<p><hr /></p>
					<article>
						<p>Trois activités de tir à l'arc sont proposées par l'association Robin des bois : </p>
						<p><ul>
							<li><a href="activite_tir_classique.php" title="Les activités :  l'arc classique">Le tir à l'arc classique</a></li>
							<li><a href="activite_tir_poulie.php" title="Les activités :  l'arc à poulie">Le tir à l'arc à poulie</a></li>
							<li><a href="activite_longbow.php" title="Les activités : le longbow">Le tir à l'arc au longbow</a></li>
						</ul></p>
					</article>
				</section>
			</div>
		</main>
			
		<footer>
				<?php include 'include/footer.php'; ?>
		</footer>
	</body>
</html>
