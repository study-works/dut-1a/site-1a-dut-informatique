<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<title>L'association | Association Robin des bois</title>
		<link rel="icon" type="image/jpg" href="images/favicon.jpg" />
		<link rel="stylesheet" href="style/general.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="style/association.css" type="text/css" media="screen" />
	</head>
	<body>
		<header>
				<?php include 'include/header.php'; ?>
		</header>

		<main>
			<div class="social">
				<?php include 'include/social.php'; ?>
			</div>
				
			<div class="aside">
				<?php include 'include/aside.php'; ?>
			</div>
			
			<div class="principal">
				<section>
					<h2>Présentation de l'association</h2>
					<p><hr /></p>
					<article>
						<p>L'<em>Association Robin des bois</em> est une association <em>Clermontoise</em> de <strong>tir à l'arc</strong>.</p>
					</article>
					
					<article>
						<h3>Les plus</h3>
						<p>Après chaque séances, une collation est offerte par l'amicale à tous ceux qui le souhaitent. Des repas bonne entente sont aussi organisés avec tous les membres tout au long de l'année.</p>
					</article>
					
					<article>
						<h3 id="membres">Nos membres</h3>
						<p>L'association est ouverte à toute personne à partir de 6 ans, et sans âge maximal limite. Il est aussi possible de vous joindre à nous malgré un handicap (même en fauteuil roulant, oui oui, vous pouvez aussi faire du tir à l'arc en fauteuil roulant).</p>
						<p><span class="attention">Attention : </span>L'association se déroule dans une bonne camaraderie, et toute personne ne respectant pas cela pourra ne plus être acceptée parmi nous.</p>
					</article>
				</section>
			</div>
		</main>
		
		<footer>
				<?php include 'include/footer.php'; ?>
		</footer>
	</body>
</html>
