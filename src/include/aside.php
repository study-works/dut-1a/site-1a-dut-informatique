<aside>
	<h4>Quelques informations complémentaires sur le tir à l'arc :</h4>
	<ul>
		<li><a href="https://fr.wikipedia.org/wiki/Tir_%C3%A0_l'arc" title="Plus d'information sur le tir à l'arc">Le tir à l'arc</a></li>
		<li><a href="https://fr.wikipedia.org/wiki/Arc_long_anglais" title="Plus d'information sur le longbow">Le Grand Arc (ou longbow)</a></li>
		<li><a href="http://www.bourges1ere.fr/images/Technique/ArcClassique.htm" title="Plus d'information sur l'arc classique">L'arc classique (ou recurve - à double courbure)</a></li>
		<li><a href="http://www.bourges1ere.fr/images/Technique/ArcAPoulies.htm" title="Plus d'information sur l'arc à poulies">L'arc à poulies (ou compound)</a></li>
	</ul>
</aside>
