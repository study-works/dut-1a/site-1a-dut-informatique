<h1 id="nom_site"><img src="images/fleche.png" class="sitef1"/>Association Robin des bois<img src="images/fleche.png" class="sitef2"/></h1>
<img src="images/arrow.png"/>
<nav>
	<ul>
		<li><a href="index.php" title="Revenir à l'accueil du site">Accueil</a></li>
		<li><a href="association.php" title="Plus d'information sur l'association">L'association</a></li>
		<li class="sous_menu"><a href="activites.php" title="Nos activités">Activités</a>
		<ul>
			<li><a href="activite_tir_classique.php" title="Les activités :  l'arc classique">Le tir à l'arc classique</a></li>
			<li><a href="activite_tir_poulie.php" title="Les activités :  l'arc à poulie">Le tir à l'arc à poulie</a></li>
			<li><a href="activite_longbow.php" title="Les activités : le longbow">Le tir à l'arc au longbow</a></li>
		</ul></li>
		<li><a href="planning.php" title="Planning 2018-2019">Planning</a></li>
		<li><a href="tarifs.php" title="Les tarifs 2018-2019">Tarifs</a></li>
		<li class="sous_menu"><a>Médias</a>
		<ul>
			<li><a href="photos.php" title="Visionner nos photos">Photos</a></li>
			<li><a href="videos.php" title="Visionner nos vidéos">Vidéos</a></li>
		</ul></li>
		<li><a href="pre_inscription.php" title="Page pour les pré-inscription">Pré-inscription</a></li>
	</ul>
</nav>
