<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<title>Matériel | Association Robin des bois</title>
		<link rel="icon" type="image/jpg" href="images/favicon.jpg" />
		<link rel="stylesheet" href="style/general.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="style/photos_taille_reel.css" type="text/css" media="screen" />
	</head>
	<body>
		<header>
				<?php include 'include/header.php'; ?>
		</header>

		<main>
			<div class="social">
				<?php include 'include/social.php'; ?>
			</div>
				
			<div class="aside">
				<?php include 'include/aside.php'; ?>
			</div>
			
			<div class="principal">
				<section>
					<h2>Arc classique</h2>
					<p><hr /></p>
					<article>
						<figure>
							<img src="photos/arc_classique/materiel.jpg" alt="Du matériel de tir" title="Du matériel de tir"/>
							<figcaption>Du matériel de tir à l'arc classique : blason - arc - flèches - carquois - ...</figcaption>
						</figure>
						<p><a href="photos.php" title="Retour à la page des photos">⇐ Retour</a></p>
					</article>
				</section>
			</div>
		</main>
			
		<footer>
				<?php include 'include/footer.php'; ?>
		</footer>
	</body>
</html>
