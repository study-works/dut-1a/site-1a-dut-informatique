<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<title>Le planning | Association Robin des bois</title>
		<link rel="icon" type="image/jpg" href="images/favicon.jpg" />
		<link rel="stylesheet" href="style/general.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="style/planning.css" type="text/css"/>
	</head>
	<body>
		<header>
				<?php include 'include/header.php'; ?>
		</header>

		<main>
			<div class="social">
				<?php include 'include/social.php'; ?>
			</div>
				
			<div class="aside">
				<?php include 'include/aside.php'; ?>
			</div>
			
			<div class="principal">
				<section>
					<h2>Le planning 2018/2019</h2>
					<p><hr /></p>
					<article>
						<p>Le planning suivant est le planning de l'association pour l'année 2018/2019, du 02 septembre 2018 au 30 juin 2019.</p>
						<p>Il dépend de l'activité choisie et du niveau d'expérience de celle-ci.</p>
					</article>
					
					<article>
						<table>
							<caption>Planning 2018/19</caption>
							<tr>
								<th>Type d'arc</th>
								<th>Niveaux</th>
								<th>Créneaux</th>
								<th>Lieux</th>
								<th></th>
							</tr>
							<tr>
								<th rowspan="4">Longbow</th>
								<td rowspan="2">Débutants</td>
								<td>Mer 18h-19h30</td>
								<td rowspan="4">Devant le gymnase</td>
								<td rowspan="4"><img src="images/planning/longbow.jpg" alt="Tir à l'arc au longbow" title="Tir à l'arc au longbow" /></td>
							</tr>
							<tr>
								<td>Dim 9h-10h30</td>
							</tr>
							<tr>
								<td rowspan="2">Confirmés</td>
								<td>Mer 19h30-21h30</td>
							</tr>
							<tr>
								<td>Dim 10h30-12h30</td>
							</tr>
							<tr>
								<th rowspan="4">Arc classique</th>
								<td rowspan="2">Débutants</td>
								<td>Mer 18h-19h30</td>
								<td rowspan="4">Boulodrome</td>
								<td rowspan="4"><img src="images/planning/classique.jpg" alt="Tir à l'arc classique" title="Tir à l'arc classique" /></td>
							</tr>
							<tr>
								<td>Dim 9h-10h30</td>
							</tr>
							<tr>
								<td rowspan="2">Confirmés</td>
								<td>Mer 19h30-21h30</td>
							</tr>
							<tr>
								<td>Dim 10h30-12h30</td>
							</tr>
							<tr>
								<th rowspan="4">Arc à poulie</th>
								<td rowspan="2">Débutants</td>
								<td>Mer 18h-19h30</td>
								<td rowspan="4">Gymnase</td>
								<td rowspan="4"><img src="images/planning/poulie.jpg" alt="Tir à l'arc à poulie" title="Tir à l'arc à poulie" /></td>
							</tr>
							<tr>
								<td>Dim 9h-10h30</td>
							</tr>
							<tr>
								<td rowspan="2">Confirmés</td>
								<td>Mer 19h30-21h30</td>
							</tr>
							<tr>
								<td>Dim 10h30-12h30</td>
							</tr>
						</table>
					</article>
				</section>
			</div>
		</main>
		
		<footer>
			<section>
				<?php include 'include/footer.php'; ?>
		</footer>
	</body>
</html>
