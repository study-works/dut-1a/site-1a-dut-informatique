<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<title>Pré-inscription | Association Robin des bois</title>
		<link rel="icon" type="image/jpg" href="images/favicon.jpg" />
		<link rel="stylesheet" href="style/general.css" type="text/css" />
		<link rel="stylesheet" href="style/pre_inscription.css" type="text/css" media="screen" />
	</head>
	<body>
		<header>
				<?php include 'include/header.php'; ?>
		</header>

		<main>
			<div class="social">
				<?php include 'include/social.php'; ?>
			</div>
				
			<div class="aside">
				<?php include 'include/aside.php'; ?>
			</div>
				
			<div class="principal">
				<section>
					<h2>Pré-inscription</h2>
					<p><hr /></p>
                    <article>
                        <?php
                        $inscr=false;

                        echo '<article class="erreur_pre_insciption">';
                            if(!empty($_POST['submit']))
                            {
                                $valid=true;
                                if(empty($_POST['name']))
                                {
                                    $valid=false;
                                    echo '<p>Le nom doit être renseigné.</p>';
                                }
                                if (empty($_POST['email']))
                                {
                                    $valid=false;
                                    echo '<p>L\'email doit être renseigné.</p>';
                                }
                                elseif (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
                                {
                                    $valid=false;
                                    echo '<p>L\'adresse email n\'est pas validee.</p>';
                                }
                                if (empty($_POST['activite']))
                                {
                                    $valid=false;
                                    echo '<p>L\'activité doit être renseignée.</p>';
                                }
                                if (empty($_POST['horraire']))
                                {
                                    $valid=false;
                                    echo '<p>Les horraires doivent être renseignées.</p>';
                                }
                                if (empty($_POST['inscription']))
                                {
                                    $valid=false;
                                    echo '<p>Vous devez indiquer s\'il s\'agit de votre première inscription ou non.</p>';
                                }
                                if (empty($_POST['mess']))
                                {
                                    $valid=false;
                                    echo '<p>Vous devez renseigner un message.</p>';
                                }
                                if($valid)
                                {
                                    $inscr=true;
                                }
                            }
                        echo ' </article>';
                        if(!$inscr)
                        {
                        ?>
                            <form method="POST" action="">
                                <p>
                                    <label for="name">Nom :</label>
                                    <input type="text" name="name" id="name" placeholder="ex : DUPOND Charle" required />
                                </p>
                                <p>
                                    <label for="email">Email :</label>
                                    <input type="email" name="email" id="email" placeholder="exemple@exemple.com" required />
                                </p>
                                <p>
                                    <label for="tel">N° de téléphone :</label>
                                    <input type="tel" name="tel" id="tel" />
                                </p>
                                <p>
                                    <label for="activite">Activité choisie :</label>
                                    <select name="activite" id="activite">
                                        <option value="classique">Tir à l'arc classique</option>
                                        <option value="poulie">Tir à l'arc à poulie</option>
                                        <option value="longbow">Tir au longbow</option>
                                    </select>
                                </p>
                                <p>
                                    <label for="horraire">Créneau horraire :</label>
                                    <select name="horraire" id="horraire">
                                        <option value="Mercredi 18h-19h30">Mercredi 18h-19h30</option>
                                        <option value="Mercredi 19h30-21h30">Mercredi 19h30-21h30</option>
                                        <option value="Dimanche 9h-10h30">Dimanche 9h-10h30</option>
                                        <option value="Mercredi 10h30-12h30">Mercredi 10h30-12h30</option>
                                    </select>
                                </p>
                                <p>
                                    <input type="radio" name="inscription" id="prem_inscr" value="oui" checked />
                                    <label for="prem_inscr">Première demande d'inscription</label>
                                    <input type="radio" name="inscription" id="deja_inscr" value="non" />
                                    <label for="deja_inscr">Déjà inscrit</label>
                                </p>
                                <p>
                                    <label for="mess">Votre message : </label>
                                    <textarea name="mess" id="mess" required></textarea>
                                </p>
                                <p>
                                    <input type="submit" name="submit" id="submit" value="Envoyer" />
                                </p>
                            </form>
                        <?php
                        }
                        if($inscr)
                        {
                            echo '<h2>Récapitulatif</h2>';
                            echo '<p>Votre demande a bien été prise en compte : </p>';
                            echo '<p><span>Nom :</span> ' . $_POST['name'] . '</p>';
                            echo '<p><span>Email :</span> ' . $_POST['email'] . '</p>';
                            echo '<p><span>N° de téléphone :</span> ' . $_POST['tel'] . '</p>';
                            echo '<p><span>Activité choisie :</span> ' . $_POST['activite'] . '</p>';
                            echo '<p><span>Horraires choisies :</span> ' . $_POST['horraire'] . '</p>';
                            echo '<p><span>Première inscription :</span> ' . $_POST['inscription'] . '</p>';
                            echo '<p><span>Votre message :</span> ' . $_POST['mess'] . '</p>';
                        }
                        ?>
					</article>
				</section>
			</div>
		</main>
			
		<footer>
				<?php include 'include/footer.php'; ?>
		</footer>
	</body>
</html>
