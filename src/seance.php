<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<title>Séance | Association Robin des bois</title>
		<link rel="icon" type="image/jpg" href="images/favicon.jpg" />
		<link rel="stylesheet" href="style/general.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="style/photos_taille_reel.css" type="text/css" media="screen" />
	</head>
	<body>
		<header>
				<?php include 'include/header.php'; ?>
		</header>

		<main>
			<div class="social">
				<?php include 'include/social.php'; ?>
			</div>
				
			<div class="aside">
				<?php include 'include/aside.php'; ?>
			</div>
				
			<div class="principal">
				<section>
					<h2>Arc classique</h2>
					<p><hr /></p>
					<article>
						<figure>
							<img src="photos/arc_classique/seance.jpg" alt="Une séance classique" title="Une séance classique"/>
							<figcaption>Une séance classique de tir à l'arc en intérieur</figcaption>
						</figure>
						<p><a href="photos.php" title="Retour à la page des photos">⇐ Retour</a></p>
					</article>
				</section>
			</div>
		</main>
			
		<footer>
				<?php include 'include/footer.php'; ?>
		</footer>
	</body>
</html>
