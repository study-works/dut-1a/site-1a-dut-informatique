<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<title>Les tarifs | Association Robin des bois</title>
		<link rel="icon" type="image/jpg" href="images/favicon.jpg" />
		<link rel="stylesheet" href="style/general.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="style/tarifs.css" type="text/css"/>
	</head>
	<body>
		<header>
				<?php include 'include/header.php'; ?>
		</header>

		<main>
			<div class="social">
				<?php include 'include/social.php'; ?>
			</div>
				
			<div class="aside">
				<?php include 'include/aside.php'; ?>
			</div>
			
			<div class="principal">
				<section>
					<h2>Les tarifs pour l'année 2018/2019</h2>
					<p><hr /></p>
					<article>
						<p>Nos tarifs évoluent de <em>1 à 4€ la séance</em>, et de </strong>40 à 85€ à l'année</strong>, en fonction de l'âge, et à partir de 6 ans.</p>
					</article>
					
					<article>
						<table>
							<caption>Tarifs</caption>
							<tr>
								<th>Âge</th>
								<th>Séance</th>
								<th>Mensuel</th>
								<th>Annuel</th>
							</tr>
							<tr>
								<th>6-12 ans</th>
								<td>1€</td>
								<td>8€</td>
								<td>40€</td>
							</tr>
							<tr>
								<th>12-16 ans</th>
								<td>2€</td>
								<td>16€</td>
								<td>60€</td>
							</tr>
							<tr>
								<th>16-20 ans</th>
								<td>3€</td>
								<td>20€</td>
								<td>75€</td>
							</tr>
							<tr>
								<th>20-35 ans</th>
								<td>4€</td>
								<td>24€</td>
								<td>85€</td>
							</tr>
							<tr>
								<th>35-50 ans</th>
								<td>3€</td>
								<td>20€</td>
								<td>75€</td>
							</tr>
							<tr>
								<th>50 ans et plus</th>
								<td>2€</td>
								<td>16€</td>
								<td>60€</td>
							</tr>
						</table>
					</article>
				</section>
			</div>
		</main>
		
		<footer>
				<?php include 'include/footer.php'; ?>
		</footer>
	</body>
</html>
